#include<stdio.h>
#include<stdlib.h>
#include "ordn.h"
	
int main(int argc, char *argv[]){
	int i;
	long array[10] = { 0, 4, 5, 8, 2, 4, 9, 23, 65, 1 };	

	printf("\nPrinting original array: \n");
	for(i = 0; i < 10; i++)
		printf("%d, ", array[i]);

	n_ordn( array, 10 );
	
	printf("\nPrinting new array: \n");
	for(i = 0; i < 10; i++)
		printf("%d, ", array[i]);

	printf("\n");

	return 0;
}
